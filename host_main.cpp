#include <iostream>
#include <stdio.h>
#include <stdexcept>
#include <iomanip>
#ifdef _WINDOWS
#include <io.h>
#else
#include <unistd.h>
#endif

#include "experimental/xrt_device.h"
#include "experimental/xrt_kernel.h"
#include "experimental/xrt_bo.h"
#include "experimental/xrt_ini.h"

#include "gpc_defs.h"
#include "leonhardx64_xrt.h"
#include "gpc_handlers.h"

static void usage()
{
	std::cout << "usage: <xclbin> <sw_kernel>\n\n";
}

int main(int argc, char** argv)
{
	unsigned int cores_count = 0;
	int size = -1;

	__foreach_core(group, core) cores_count++;

	//Assign xclbin
	if (argc < 3) {
		usage();
		throw std::runtime_error("FAILED_TEST\nNo xclbin specified");
	}

	//Open device #0
	leonhardx64 lnh_inst = leonhardx64(0,argv[1]);
	__foreach_core(group, core)
	{
		lnh_inst.load_sw_kernel(argv[2], group, core);
	}

	// Создание таблиц
	__foreach_core(group, core)
	{
		lnh_inst.gpc[group][core]->start_async(__event__(create_free_table));
	}

	// Выделение страницы
	__foreach_core(group, core) 
	{
		lnh_inst.gpc[group][core]->start_async(__event__(allocate_page));
	}

	// Получение размера занятой страницы до выделения
	__foreach_core(group, core) {
		size = lnh_inst.gpc[group][core]->mq_receive();
	}

	std::cout << "size before = " << size << std::endl;

	// Получение размера занятой страницы после выделения
	__foreach_core(group, core) {
		size = lnh_inst.gpc[group][core]->mq_receive();
	}

	std::cout << "size after = " << size << std::endl;

	// Освобождение страницы
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->start_async(__event__(free_page));
	}

	printf("Тест пройден успешно!\n");
	return 0;
}
