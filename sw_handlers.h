#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "sw-kernel-lib/lib/lnh64.h"
#include "sw-kernel-lib/lib/gpc_io_swk.h"

//Макросы для формирования структур inline 
#define STRUCT(type, ...) typedef union { struct __VA_ARGS__ __struct; uint64_t bits; } type
#define INLINE(type,...) (((type){__VA_ARGS__}).bits)

//Структуры данных
#define 		FREE_PAGE 	1 	
#define 		BUSY_PAGE 	2

// Константы 
#define COUNT_PAGES 1048575

STRUCT(
page_key, {
        uint64_t               index        :64;
});

STRUCT(
page_value, {
        uint64_t               atr          :64;
});
