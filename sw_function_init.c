#include "sw_handlers.h"

extern lnh lnh_core;
extern global_memory_io gmio;

// Cформировать таблицы
void create_free_table() {
    lnh_del_str_async(FREE_PAGE);
    lnh_del_str_async(BUSY_PAGE);
    for (uint64_t i; i < COUNT_PAGES; ++i) {
        lnh_ins_sync(FREE_PAGE, INLINE(page_key, {.index=i}), INLINE(page_value, {.atr=0}));
    }
    lnh_sync();
}

// Выделить страницу
void allocate_page() {
    mq_send(lnh_get_num(FREE_PAGE));
    lnh_get_first(FREE_PAGE);
    uint64_t index = lnh_core.result.key;
    lnh_del_sync(FREE_PAGE, lnh_core.result.key);
    lnh_ins_sync(BUSY_PAGE, INLINE(page_key, {.index=index}), INLINE(page_value, {.atr=0}));
    mq_send(lnh_get_num(FREE_PAGE));
}

// Освободить страницу
void free_page() {
    lnh_get_first(BUSY_PAGE);
    uint64_t index = lnh_core.result.key;
    lnh_del_sync(BUSY_PAGE, lnh_core.result.key);
    lnh_ins_sync(FREE_PAGE, INLINE(page_key, {.index=index}), INLINE(page_value, {.atr=0}));
}
